package com.pornchitar.week11;

public interface Moveable {
    public void left();
    public void right();
    public void up();
    public void down();
}
