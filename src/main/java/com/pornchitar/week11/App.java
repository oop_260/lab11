package com.pornchitar.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.eat();
        clark.sleep();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.swim();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat bat1 = new Bat("Bat");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Snake snake = new Snake("Snake");
        snake.eat();
        snake.sleep();
        snake.crawl();
        snake.swim();
        Rat rat1 = new Rat("Rat");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        rat1.swim();
        Dog dog1 = new Dog("Dog");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();
        Cat cat1 = new Cat("Cat");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        cat1.swim();
        Crocodile crocodile1 = new Crocodile("Crocodile");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        crocodile1.swim();
        Fish fish1 = new Fish("Fish");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        Submarine Lodbrog = new Submarine("Lodbrog", "Diesel SKL Magdeburg");
        Lodbrog.left();
        Lodbrog.right();
        Lodbrog.up();
        Lodbrog.down();
        Lodbrog.swim();
        Bus bus = new Bus("bus", "Volvo B11R");
        bus.left();
        bus.right();
        bus.up();
        bus.down();

        Flyable[] flyables = {bird1, boeing, clark, bat1};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = {bird1, clark, man1, rat1, dog1, cat1};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Crawlable[] crawlable = {snake, crocodile1};
        for (int i = 0; i < crawlable.length; i++) {
            crawlable[i].crawl();
        }

        Swimable[] swimables = {clark, man1, snake, rat1, dog1, cat1, crocodile1, fish1, Lodbrog};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        Moveable[] moveables = {Lodbrog, bus};
        for (int i = 0; i < moveables.length; i++) {
            moveables[i].left();
            moveables[i].right();
            moveables[i].up();
            moveables[i].down();
        }
    }
}
