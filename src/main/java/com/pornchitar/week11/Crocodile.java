package com.pornchitar.week11;

public class Crocodile extends Animal implements Crawlable, Swimable{
    public Crocodile(String name) {
        super(name, 0);
    }
    
    @Override
    public String toString() {
        return "Crocodile ("+this.getName()+")";
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+" sleep.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+" eat.");
        
    }
    @Override
    public void crawl() {
        System.out.println(this.toString()+" crawl.");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString()+" swim.");
        
    }
}
