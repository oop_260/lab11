package com.pornchitar.week11;

public class Submarine extends Vehicle implements Moveable, Swimable{

    public Submarine(String name, String engine) {
        super(name, engine);
    }
    @Override
    public String toString(){
        return "Submarine("+getName()+")";
    }

    @Override
    public void left() {
        System.out.println(this +" left.");
        
    }

    @Override
    public void right() {
        System.out.println(this +" right.");
        
    }

    @Override
    public void up() {
        System.out.println(this +" up.");
        
    }

    @Override
    public void down() {
        System.out.println(this +" down.");
        
    }
    @Override
    public void swim() {
        System.out.println(this.toString()+" swim.");
        
    }
    
}
